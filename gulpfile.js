const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const ifElse = require('gulp-if-else');
const argv = require('yargs').argv;

gulp.task('images', () => {
  gulp.src(['src/img/**/*.{jpg,jpeg,png,gif}'], { base: 'src/img' })
    .pipe(ifElse(argv.compress, () => imagemin()))
    .pipe(gulp.dest('dist/img'));
});
