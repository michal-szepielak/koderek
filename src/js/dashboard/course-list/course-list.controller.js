angular.module('app.dashboard.courseList', [
    'ui.router',
    'app.common.services.student'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('dashboard.courseList', {
        url: '/course-list/:parentId',
        templateUrl: 'js/dashboard/course-list/_course-list.html',
        controller: 'CourseListController'
    });
}])

.controller('CourseListController', ['$scope', '$state', '$window', 'studentService', 'courseService', function ($scope, $state, $window, studentService, courseService) {
    studentService.getAllForParent($state.params.parentId).then(students => {
        const init = () => {
            $scope.children = students;

            $scope.children.forEach(student => {
                courseService.getAllForStudent($state.params.parentId, student.id).then(courses => {
                    student.courses = courses;
                });

                courseService.getAllAvailableForStudent($state.params.parentId, student.id).then(courses => {
                    student.availableCourses = courses;
                })
            });
        };

        init();



        $scope.register = (course, student) => {
            courseService.register(course.id, student.id, $state.params.parentId).then(() => {
                init();
            });
        };
    });
}]);
