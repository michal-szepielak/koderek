angular.module('app.admin.parents')
.controller('NewParentModalController',  function ($uibModalInstance, item, parentService) {
    var $ctrl = this;
    $ctrl.item = item;

    $ctrl.ok = function () {
        parentService.createNew(item).then((savedItem) => {
            $uibModalInstance.close(item);
        }, (result) => {
            console.log(result);
        });
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
