angular.module('app.dashboard.family', [
    'ui.router'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('dashboard.family', {
        url: '/family',
        templateUrl: 'js/dashboard/family/_family.html',
        controller: 'FamilyController'
    });
}])

.controller('FamilyController', ['$scope', '$state', '$window', function ($scope, $state, $window) {




}]);