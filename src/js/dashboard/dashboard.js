import './course-list/course-list.controller';

angular.module('app.dashboard', [
    'app.dashboard.courseList'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('dashboard', {
        url: '/dashboard',
        abstract: true,
        templateUrl: 'js/dashboard/_dashboard.html',
    });
}]);
