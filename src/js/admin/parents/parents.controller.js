angular.module('app.admin.parents', [
    'ui.router',
    'app.common.services.parent',
    'app.common.models.Parent'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('admin.parents', {
        url: '/parents',
        templateUrl: 'js/admin/parents/_parents.html',
        controller: 'ParentsController',
        resolve: {
            parentList: ['parentService', function (parentService) {
                return parentService.getAll();
            }]
        },
    });
}])

.controller('ParentsController', ['$scope', '$state', '$window', '$uibModal', 'parentService', 'parentList', 'parentModel', function ($scope, $state, $window, $uibModal, parentService, parentList, Parent) {
    let $ctrl = this;
    $ctrl.items = ['item1', 'item2', 'item3'];

    $scope.add = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'js/admin/parents/_new_parent.modal.html',
            controller: 'NewParentModalController',
            controllerAs: '$ctrl',
            size: 'lg',
            appendTo: parentElem,
            resolve: {
                item: function () {
                    return new Parent();
                }
            }
        });

        modalInstance.result.then(function (item) {
            // $ctrl.selected = selectedItem;
            $scope.parentList.push(item);
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.parentList = parentList;
}]);