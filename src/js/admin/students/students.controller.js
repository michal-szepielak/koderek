angular.module('app.admin.students', [
    'ui.router',
    'app.common.services.parent',
    'app.common.services.group',
    'app.common.services.student',
    'app.common.models.Student'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('admin.students', {
        url: '/students',
        templateUrl: 'js/admin/students/_students.html',
        controller: 'StudentsController',
        resolve: {
            list: ['studentService', function (studentService) {
                return studentService.getAll();
            }],
            parents: function(parentService) {
                return parentService.getAll();
            },
            groups: (groupService) => {
                return groupService.getAll();
            }
        }
    });
}])

.controller('StudentsController', ['$scope', '$state', '$window', '$uibModal', 'parentService', 'groupService', 'list', 'studentModel', 'studentService', 'parents', 'groups', function ($scope, $state, $window, $uibModal, parentService, groupService, list, Student, studentService, parents, groups) {
    let $ctrl = this;

    $scope.add = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'js/admin/students/_new_student.modal.html',
            controller: 'NewStudentModalController',
            controllerAs: '$ctrl',
            size: 'lg',
            appendTo: parentElem,
            resolve: {
                item: function () {
                    return new Student();
                },
                parents: function () {
                    return parentService.getAll();
                },
                groups: () => {
                    return groupService.getAll();
                }
            }
        });

        modalInstance.result.then(function (item) {
            // $ctrl.selected = selectedItem;
            $scope.list.push(item);
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

    let groupsById = [];
    let parentsById = [];

    groups.forEach((g) => {
        groupsById[g.id] = g;
    });

    parents.forEach((g) => {
        parentsById[g.id] = g;
    });

    list.forEach((student) => {
        student.group = groupsById[student.groupsId];
        student.parent = parentsById[student.parentId];
    });

    $scope.list = list;
}]);