angular.module('app.admin.students')
.controller('NewStudentModalController', function ($uibModalInstance, item, parents, groups, studentService) {
    var $ctrl = this;
    $ctrl.item = item;
    $ctrl.groups = groups;
    $ctrl.parents = parents;

    $ctrl.ok = function () {
        studentService.createNew(item).then((savedItem) => {
            $uibModalInstance.close(item);
        }, (result) => {
            console.log(result);
        });
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
