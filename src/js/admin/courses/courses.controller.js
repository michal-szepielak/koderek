angular.module('app.admin.courses', [
    'ui.router',
    'app.common.services.course',
    'app.common.models.Course'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('admin.courses', {
        url: '/',
        templateUrl: 'js/admin/courses/_courses.html',
        controller: 'CoursesController',
        resolve: {
            courseList: ['courseService', function (courseService) {
                return courseService.getAll();
            }]
        },
    });
}])

.controller('CoursesController', ['$scope', '$state', '$window', '$uibModal', 'courseService', 'courseList', 'courseModel', function ($scope, $state, $window, $uibModal, courseService, courseList, Course) {
    let $ctrl = this;

    $scope.add = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'js/admin/courses/_new_course.modal.html',
            controller: 'NewCourseModalController',
            controllerAs: '$ctrl',
            size: 'lg',
            appendTo: parentElem,
            resolve: {
                item: function () {
                    return new Course();
                }
            }
        });

        modalInstance.result.then(function (item) {
            // $ctrl.selected = selectedItem;
            $scope.courseList.push(item);
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.courseList = courseList;
}]);