angular.module('app.admin.courses')
.controller('NewCourseModalController', function ($uibModalInstance, item, courseService) {
    var $ctrl = this;

    $ctrl.item = item;

    $ctrl.ok = function () {
        courseService.createNew(item).then((savedItem) => {
            $uibModalInstance.close(item);
        }, (result) => {
            console.log(result);
        });
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
