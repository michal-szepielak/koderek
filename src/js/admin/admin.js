import './courses/courses.controller';
import './courses/new-course.modal';

import './parents/parents.controller';
import './parents/new-parent.modal';

import './groups/groups.controller';
import './groups/new-group.modal';

import './students/students.controller';
import './students/new-student.modal';


angular.module('app.admin', [
    'app.admin.courses',
    'app.admin.parents',
    'app.admin.groups',
    'app.admin.students'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('admin', {
        url: '/admin',
        abstract: true,
        templateUrl: 'js/admin/_admin.html',
    });
}]);
