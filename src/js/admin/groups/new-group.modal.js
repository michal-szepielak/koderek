

angular.module('app.admin.groups')
.controller('NewGroupModalController', function ($uibModalInstance, item, groupService) {
    var $ctrl = this;
    $ctrl.item = item;

    $ctrl.ok = function () {
        groupService.createNew(item).then((savedItem) => {
            $uibModalInstance.close(item);
        }, (result) => {
            console.log(result);
        });
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
