angular.module('app.admin.groups', [
    'ui.router',
    'app.common.services.group',
    'app.common.models.Group'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('admin.groups', {
        url: '/groups',
        templateUrl: 'js/admin/groups/_groups.html',
        controller: 'GroupsController',
        resolve: {
            groupList: ['groupService', function (groupService) {
                console.log('leci');
                return groupService.getAll();
            }]
        },
    });
}])

.controller('GroupsController', ['$scope', '$state', '$window', '$uibModal', 'groupService', 'groupList', 'groupModel', function ($scope, $state, $window, $uibModal, groupService, groupList, Group) {
    let $ctrl = this;
    $ctrl.items = ['item1', 'item2', 'item3'];

    $scope.add = function (size, parentSelector) {
        var parentElem = parentSelector ?
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
        var modalInstance = $uibModal.open({
            animation: false,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'js/admin/groups/_new_group.modal.html',
            controller: 'NewGroupModalController',
            controllerAs: '$ctrl',
            size: 'lg',
            appendTo: parentElem,
            resolve: {
                item: function () {
                    return new Group();
                }
            }
        });

        modalInstance.result.then(function (item) {
            $scope.groupList.push(item);
        }, function () {
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.groupList = groupList;
}]);