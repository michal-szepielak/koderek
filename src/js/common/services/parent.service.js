angular.module('app.common.services.parent', [
    'app.common.models.Parent'
])
.service('parentService', ['parentModel', '$http', '$q', 'config', function (Parent, $http, $q, config) {
    const serverUrl = config.serverUrl;
    const namespace = 'admin';


    this.getAll = () => {
        const defer = $q.defer();

        $http.get(`${serverUrl}/${namespace}/parents`).then((result) => {
            let parents = [];

            result.data.forEach((parent) => {
                parents.push(new Parent(parent));
            });

            defer.resolve(parents);
        }, (error) => {
            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

    this.createNew = (item) => {
        const defer = $q.defer();

        $http.post(`${serverUrl}/${namespace}/parents`, item.serialize()).then((result) => {
            let items = [];

            // result.data.forEach((item) => {
            //     items.push(item);
            //     //items.push(new Course(item));
            // });

            defer.resolve(result);
        }, (error) => {

            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };



}]);