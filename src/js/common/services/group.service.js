angular.module('app.common.services.group', [
    'app.common.models.Group'
])
.service('groupService', ['groupModel', '$http', '$q', 'config', function (Group, $http, $q, config) {
    const serverUrl = config.serverUrl;
    const namespace = 'admin';


    this.getAll = () => {
        const defer = $q.defer();

        $http.get(`${serverUrl}/${namespace}/groups`).then((result) => {
            let groups = [];

            result.data.forEach((group) => {
                groups.push(new Group(group));
            });

            defer.resolve(groups);
        }, (error) => {
            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

    this.createNew = (item) => {
        const defer = $q.defer();

        $http.post(`${serverUrl}/${namespace}/groups`, item.serialize()).then((result) => {
            let items = [];

            // result.data.forEach((item) => {
            //     items.push(item);
            //     //items.push(new Course(item));
            // });

            defer.resolve(result);
        }, (error) => {

            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

}]);