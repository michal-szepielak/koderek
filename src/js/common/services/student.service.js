angular.module('app.common.services.student', [
    'app.common.models.Student'
])
.service('studentService', ['studentModel', '$http', '$q', 'config', function (Student, $http, $q, config) {
    const serverUrl = config.serverUrl;
    const namespace = 'admin';


    this.getAll = (groups, parents) => {
        const defer = $q.defer();

        $http.get(`${serverUrl}/${namespace}/students`).then((result) => {
            let parents = [];

            result.data.forEach((group) => {
                parents.push(new Student(group));
            });

            defer.resolve(parents);
        }, (error) => {
            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

    this.createNew = (item) => {
        const defer = $q.defer();

        $http.post(`${serverUrl}/${namespace}/students`, item.serialize()).then((result) => {
            let items = [];

            // result.data.forEach((item) => {
            //     items.push(item);
            //     //items.push(new Course(item));
            // });

            defer.resolve(result);
        }, (error) => {

            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

this.getAllForParent = (parentId) => {
        const defer = $q.defer();
        $http.get(`${serverUrl}/parent/${parentId}/students`).then((result) => {
            let students = [];

            result.data.forEach((student) => {
                students.push(new Student(student));
            });

            defer.resolve(students);
        }, (error) => {
            console.error(error);
            defer.reject();
        });
        return defer.promise;

    }



}]);
