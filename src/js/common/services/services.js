import './course.service';
import './parent.service';
import './group.service';
import './student.service';

angular.module('app.common.services', [
    'app.common.services.course',
    'app.common.services.parent',
    'app.common.services.group',
    'app.common.services.student'
]);