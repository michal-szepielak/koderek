angular.module('app.common.services.course', [
    'app.common.models.Course'
])
.service('courseService', ['courseModel', '$http', '$q', 'config', function (Course, $http, $q, config) {
    const serverUrl = config.serverUrl;
    const namespace = 'admin';


    this.getAll = () => {
        const defer = $q.defer();

        $http.get(`${serverUrl}/${namespace}/courses`).then((result) => {
            let items = [];

            result.data.forEach((item) => {
                items.push(new Course(item));
            });

            defer.resolve(items);
        }, (error) => {
            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

    this.createNew = (item) => {
        const defer = $q.defer();

        $http.post(`${serverUrl}/${namespace}/courses`, item.serialize()).then((result) => {
            let items = [];

            // result.data.forEach((item) => {
            //     items.push(item);
            //     //items.push(new Course(item));
            // });

            defer.resolve(result);
        }, (error) => {

            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

    this.register = (courseId, studentId, parentId) => {
        const defer = $q.defer();

        $http.post(`${serverUrl}/parent/${parentId}/students/${studentId}/courses/${courseId}/sign`, {}).then((result) => {
            defer.resolve(result);
        }, (error) => {

            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

    this.getAllForStudent = (parentId, studentId) => {
        const defer = $q.defer();

        $http.get(`${serverUrl}/parent/${parentId}/students/${studentId}/courses`).then((result) => {
            let items = [];

            result.data.forEach((item) => {
                items.push(new Course(item));
            });

            defer.resolve(items);
        }, (error) => {
            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };

    this.getAllAvailableForStudent = (parentId, studentId) => {
        const defer = $q.defer();

        $http.get(`${serverUrl}/parent/${parentId}/students/${studentId}/availableCourses`).then((result) => {
            let items = [];

            result.data.forEach((item) => {
                items.push(new Course(item));
            });

            defer.resolve(items);
        }, (error) => {
            console.error(error);
            defer.reject();
        });

        return defer.promise;
    };



    
}]);