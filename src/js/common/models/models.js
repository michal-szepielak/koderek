import Course from './Course';
import Group from './Group';
import Parent from './Parent';
import Student from './Student';

angular.module('app.common.models', [
    'app.common.models.Course',
    'app.common.models.Group',
    'app.common.models.Parent',
    'app.common.models.Student'
]);

angular.module('app.common.models.Parent', [])
    .factory('parentModel', function () {
        return Parent;
    });

angular.module('app.common.models.Course', [])
    .factory('courseModel', function () {
        return Course;
    });

angular.module('app.common.models.Group', [])
    .factory('groupModel', function () {
        return Group;
    });

angular.module('app.common.models.Student', [])
    .factory('studentModel', function () {
        return Student;
    });