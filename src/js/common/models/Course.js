function formatDate(d) {
    let date = new Date(d);

    return date.toISOString().split('.').shift();
}

class Course {
    constructor(options) {
        options = options || {};
        this._id = options.id;
        this._name = options.name;
        this._description = options.description;
        this._createDate = options.createDate;
        this._signUpStartDate = options.signUpStartDate;
        this._signUpEndDate = options.signUpEndDate;
        this._rate = options.rate;
        this._host = options.host;
        this._slots = options.slots;
        this._weekDay = options.weekDay;
        this._startTime = options.startTime;
        this._stopTime = options.stopTime;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get description() {
        return this._description;
    }

    set description(value) {
        this._description = value;
    }

    get createDate() {
        return this._createDate;
    }

    set createDate(value) {
        this._createDate = value;
    }

    get signUpStartDate() {
        return this._signUpStartDate;
    }

    set signUpStartDate(value) {
        this._signUpStartDate = value;
    }

    get signUpEndDate() {
        return this._signUpEndDate;
    }

    set signUpEndDate(value) {
        this._signUpEndDate = value;
    }

    get rate() {
        return this._rate;
    }

    set rate(value) {
        this._rate = value;
    }

    get host() {
        return this._host;
    }

    set host(value) {
        this._host = value;
    }

    get slots() {
        return this._slots;
    }

    set slots(value) {
        this._slots = value;
    }

    get weekDay() {
        return this._weekDay;
    }

    set weekDay(value) {
        this._weekDay = value;
    }

    get startTime() {
        return this._startTime;
    }

    set startTime(value) {
        this._startTime = value;
    }

    get stopTime() {
        return this._stopTime;
    }

    set stopTime(value) {
        this._stopTime = value;
    }

    serialize() {
        return {
            id: this._id,
            name: this._name,
            description: this._description,
            createDate: this._createDate,
            signUpStartDate: formatDate(this._signUpStartDate),
            signUpEndDate: formatDate(this._signUpEndDate),
            rate: this._rate,
            host: this._host,
            slots: this._slots,
            weekDay: this._weekDay,
            startTime: formatDate(this._startTime),
            stopTime: formatDate(this._stopTime),
        }
    }
}

module.exports = Course;
