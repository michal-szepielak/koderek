"use strict";

class Parent {
    constructor(options) {
        options = options || {};
        this._id = options.id;
        this._firstName = options.firstName;
        this._lastName = options.lastName;
        this._email = options.email;
        this._password = options.password;
        this._active = options.active;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get firstName() {
        return this._firstName;
    }

    set firstName(value) {
        this._firstName = value;
    }

    get lastName() {
        return this._lastName;
    }

    set lastName(value) {
        this._lastName = value;
    }

    get email() {
        return this._email;
    }

    set email(value) {
        this._email = value;
    }

    get password() {
        return this._password;
    }

    set password(value) {
        this._password = value;
    }

    get active() {
        return this._active;
    }

    set active(value) {
        this._active = value;
    }

    /**
     * Returns save to compromise data
     * @returns {{id: *, firstName: *, lastName: *, email: *, active: *}}
     */
    serialize() {
        return {
            id: this.id,
            firstName: this.firstName,
            lastName: this.lastName,
            email: this.email,
            active: this.active
        };
    }
}

module.exports = Parent;