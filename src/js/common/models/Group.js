"use strict";

class Group {
    constructor(options) {
        options = options || {};
        this._id = options.id;
        this._name = options.name;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    serialize() {
        return {
            id: this._id,
            name: this._name
        }
    }
}

module.exports = Group;