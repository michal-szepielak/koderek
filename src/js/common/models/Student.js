"use strict";

class Student {
    constructor(options, group, parent) {
        options = options || {};
        this._id = options.id;
        this._firstName = options.firstName;
        this._lastName = options.lastName;
        this._parent = options.group;
        this._group = options.parent;
        this.parentId = options.parentId;
        this.groupsId = options.groupsId;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get firstName() {
        return this._firstName;
    }

    set firstName(value) {
        this._firstName = value;
    }

    get lastName() {
        return this._lastName;
    }

    set lastName(value) {
        this._lastName = value;
    }

    get parent() {
        return this._parent;
    }

    set parent(value) {
        this._parent = value;
    }

    get group() {
        return this._group;
    }

    set group(value) {
        this._group = value;
    }

    /**
     * Returns save to compromise data
     * @returns {{id: *, firstName: *, lastName: *, email: *, active: *}}
     */
    serialize() {
        return {
            id: this.id,
            firstName: this.firstName,
            lastName: this.lastName,
            parentId: String(this.parent.id),
            groupsId: String(this.group.id)
        };
    }
}

module.exports = Student;