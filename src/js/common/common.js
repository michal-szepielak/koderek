import './models/models'
import './services/services'

angular.module('app.common', [
    'app.common.models',
    'app.common.services'
]);