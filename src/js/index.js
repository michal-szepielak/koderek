
// import 'foundation-sites/dist/js/foundation';
import 'bootstrap';
import 'angular';
import 'angular-ui-bootstrap';
import 'angular-ui-router';

import './app';
import './users/users';
import './dashboard/dashboard';
import './admin/admin';
import './common/common';

import '../css/style.scss';