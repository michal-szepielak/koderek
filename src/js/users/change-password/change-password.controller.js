angular.module('app.users.changePassword', [
    'ui.router'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('users.changePassword', {
        url: '/change-password',
        templateUrl: 'js/users/change-password/_change-password.html',
        controller: 'ChangePasswordController'
    });
}])

.controller('ChangePasswordController', ['$scope', '$state', '$window', function ($scope, $state, $window) {
  var form = {
    email: '',
    password: ''
  };

  function resetForm() {
    $scope.form = angular.copy(form);
    $scope.errorMessage = '';
  }

  resetForm();

  $scope.login = function () {
      console.log('login hohohoho');
    // session.loginUser($scope.form.email, $scope.form.password).then(function () {
    //   $state.go('dashboard.your-habits');
    // }, function (response) {
    //   $scope.isFormError = true;
    //   $scope.errorMessage = response.data.error;
    // });
  };

}]);