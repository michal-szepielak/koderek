import './sign-in/sign-in.controller';
import './change-password/change-password.controller';

angular.module('app.users', [
    'app.users.signIn',
    'app.users.changePassword'
])
.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('users', {
        url: '/users',
        abstract: true,
        template: '<ui-view />'
    });
}]);
console.log('user');