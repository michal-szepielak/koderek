angular.module('app', [
    'ui.router',
    'ui.bootstrap',

    // App modules
    'app.users',
    'app.dashboard',
    'app.admin',
    'app.common'
]).constant('config', {
    serverUrl: 'http://localhost:3000'
})
    .config(['$urlRouterProvider', function ($urlRouterProvider) {
        $urlRouterProvider.otherwise('/dashboard/course-list');
    }])
    .run(['$rootScope', '$state', function ($rootScope, $state) {
        $rootScope.$on('$stateChangeStart', function (e, toState) {
            var publicStates = [
                'users.signIn',
                'users.register',
                'users.passwordReset',
                'users.passwordEdit',
                'dashboard.profilePage'
            ];

            if (publicStates.indexOf(toState.name) >= 0) {
                return;
            }

            // if (!session.isLoggedIn()) {
            //     // Try to restore session
            //     session.getCurrentUser().catch(function () {
            //         e.preventDefault();
            //         $state.go('users.signIn');
            //     });
            // }
        });

        // Redirect all unauthorized requests.
        // $rootScope.$on('error:401', function (response) {
        //     session.resetSession();
        //     $state.go('users.signIn');
        // });

        // $rootScope.session = session;

        console.dir($state.get());
    }]);
