"use strict";

const slashDB = require("../lib/services/slashDB");

/**
 *
 * @param parentId
 * @param studentId
 * @returns {Promise.<boolean>}
 */
const isParentOfStudent = (parentId, studentId) => {
    return slashDB.student.getParentId(studentId).then(parentIdOfStudent => {
        return Promise.resolve(parseInt(parentIdOfStudent) === parentId);
    });
};

const isParentOfChild = (req, res, next) => {
      isParentOfStudent(parseInt(req.params.parentId), parseInt(req.params.studentId)).then(isParent => {
          isParent ? next() : res.status(403).send({
              message: "could not get - you are not student's parent"
          });
      })
};

module.exports = isParentOfChild;