"use strict";

const slashDB = require("../../../lib/services/slashDB");
const helpers = require("./helpers");

/* GET student details. */
const getRequestHandler = (req, res) => {
    slashDB.course.getAllForStudent(req.params.studentId).then(courses => {
        res.send(courses);
    }).catch(e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const getSingleRequestHandler = (req, res) => {
    slashDB.course.get(req.params.courseId).then(course => {
        res.send(course);
    }).catch(e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const doesNotOverlap = (newCourse, existingCourse) => {
    if (newCourse.weekDay !== existingCourse.weekDay) {
        return true;
    }

    const newCourseTimeAsJson = {
        startTime: helpers.getTimeAsJson(newCourse.startTime),
        endTime: helpers.getTimeAsJson(newCourse.endTime)
    };
    const existingCourseTimeAsJson = {
        startTime: helpers.getTimeAsJson(existingCourse.startTime),
        endTime: helpers.getTimeAsJson(existingCourse.endTime)
    };

    if (newCourseTimeAsJson.startTime.hours > existingCourseTimeAsJson.endTime.hours) {
        return true;
    }
    if (existingCourseTimeAsJson.startTime.hours > newCourseTimeAsJson.endTime.hours) {
        return true;
    }
    if (existingCourseTimeAsJson.startTime.hours === newCourseTimeAsJson.endTime.hours) {
        if (newCourseTimeAsJson.endTime.minutes <= existingCourseTimeAsJson.startTime.minutes) {
            return true;
        }
    }
    if (newCourseTimeAsJson.startTime.hours === existingCourseTimeAsJson.endTime.hours) {
        if (existingCourseTimeAsJson.endTime.minutes <= newCourseTimeAsJson.startTime.minutes) {
            return true;
        }
    }

    return false;
};

const isSignUpOpen = course => {
    const now = new Date();

    return (new Date(course.signUpStartDate) < now) && (new Date(course.signUpEndDate) > now);
};

const getAvailableCourses = studentId => {
    return slashDB.course.getAvailableForStudentGroup(studentId).then(courses => {
        return slashDB.course.getAllForStudent(studentId).then(signedCourses => {
            const ids = signedCourses.map(course => {
                return course.id;
            });

            return courses.filter(course => {
                // is not signed up
                return ids.indexOf(course.id) === -1;
            }).filter(course => {
                // is able to sign up (slot is available)
                return course.participants.length < course.slots * 2; //twice as much - we can make "queue" for courses
            }).filter(course => {
                let doesOverlap = false;
                signedCourses.forEach(signedCourse => {
                    doesOverlap = doesNotOverlap(course, signedCourse) ? doesOverlap : true;
                });

                return !doesOverlap;
            }).filter(course => {
                return isSignUpOpen(course);
            });
        });
    });
};

const getAvailableRequestHandler = (req, res) => {
    getAvailableCourses(req.params.studentId).then(courses => {
        res.send(courses);
    }).catch(e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const signStudent = (req, res) => {
    // need to take care of transaction, therefore sync http request to get & save student
    getAvailableCourses(req.params.studentId).then(courses => {
        const courseToSignUp = courses.filter(course => {
            return course.id === parseInt(req.params.courseId);
        });

        if (courseToSignUp.length) {
            const slotAvailability = slashDB.course.getSlotAvailability(req.params.courseId);

            if (slotAvailability.availableMainSlots > 0) {
                slashDB.course.signUpToMainSlot(req.params.courseId, req.params.studentId);
                res.status(201).send({
                    message: "Ok",
                    type: 0
                });
            }
            else if (slotAvailability.availableQueueSlots > 0) {
                slashDB.course.signUpToQueueSlot(req.params.courseId, req.params.studentId);
                res.status(201).send({
                    message: "Ok",
                    type: 1
                });
            }
            else {
                res.status(444).send({
                    message: "Could not sign up to the course - the class is full."
                });
            }
        }
        else {
            res.status(403).send({
                message: "Student is not eligible to sign up to the course."
            });
        }
    });
};

module.exports.get = getRequestHandler;
module.exports.getAvailable = getAvailableRequestHandler;
module.exports.getSingle = getSingleRequestHandler;
module.exports.signStudent = signStudent;