"use strict";

const getTimeAsJson = time => {
    // time: 2017-01-01T11:00:00
    time = time.substring(time.indexOf("T") + 1);


    return {
        hours: parseInt(time.substring(0, 2)),
        minutes: parseInt(time.substring(3, 5))
    };
};

module.exports.getTimeAsJson = getTimeAsJson;