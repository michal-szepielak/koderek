"use strict";

const slashDB = require("../../../lib/services/slashDB");

/* GET students list. */
const getRequestHandler = (req, res) => {
    slashDB.student.getAllForParent(req.params.parentId).then(students => {
        res.send(students);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const getSingleRequestHandler = (req, res) => {
    slashDB.student.get(req.params.studentId).then(student => {
        res.send(student);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

module.exports.get = getRequestHandler;
module.exports.getSingle = getSingleRequestHandler;
