"use strict";

const express = require('express');
const router = express.Router();

const students = require("./students/students");
const courses = require("./courses/courses");


const slashDB = require("../../lib/services/slashDB");

const Parent = require("../../lib/models/Parent");

router.get("/:parentId", (req, res) => {
    slashDB.parent.get(req.params.parentId).then(parentOptions => {
        const parent = new Parent(parentOptions);

        res.send(parent.serialize());
    });
});

router.get("/:parentId/students/", students.get);
router.get("/:parentId/students/:studentId", students.getSingle);

router.get("/:parentId/students/:studentId/courses", courses.get);
router.get("/:parentId/students/:studentId/courses/:courseId", courses.getSingle);

router.get("/:parentId/students/:studentId/availableCourses", courses.getAvailable);

router.post("/:parentId/students/:studentId/courses/:courseId/sign", courses.signStudent);


module.exports = router;
