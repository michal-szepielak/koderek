"use strict";

const slashDB = require("../../../lib/services/slashDB");

const Parent = require("../../../lib/models/Parent");

/* GET parents list. */
const getRequestHandler = (req, res) => {
    slashDB.parent.getAllWithStudents().then(parents => {
        parents.forEach(parent => {
            parent.students = parent.students_parentId.slice();

            delete parent.students_parentId;
        });

        res.send(parents);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const postRequestHandler = (req, res) => {
    slashDB.parent.create(req.body).then(response => {
        console.dir(response);
        res.status(201).send({
            message: "Parent created"
        });
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const getSingleRequestHandler = (req, res) => {
    slashDB.parent.get(req.params.parentId).then(parentOptions => {
        const parent = new Parent(parentOptions);

        res.send(parent.serialize());
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

module.exports.get = getRequestHandler;
module.exports.post = postRequestHandler;
module.exports.getSingle = getSingleRequestHandler;
