"use strict";

const slashDB = require("../../../lib/services/slashDB");

/* GET courses list. */
const getRequestHandler = (req, res) => {
    slashDB.course.getAll().then(courses => {
        res.send(courses);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const postRequestHandler = (req, res) => {
    slashDB.course.create(req.body).then(response => {
        console.dir(response);
        res.status(201).send({
            message: "Course created"
        });
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const getSingleRequestHandler = (req, res) => {
    slashDB.course.get(req.params.courseId).then(course => {
        res.send(course);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

module.exports.get = getRequestHandler;
module.exports.post = postRequestHandler;
module.exports.getSingle = getSingleRequestHandler;