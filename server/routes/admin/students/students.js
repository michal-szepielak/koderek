"use strict";

const slashDB = require("../../../lib/services/slashDB");

/* GET students list. */
const getRequestHandler = (req, res) => {
    slashDB.student.getAll().then(students => {
        res.send(students);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const getSingleRequstHandler = (req, res) => {
    slashDB.student.get(req.params.studentId).then(student => {
        res.send(student);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const postRequestHandler = (req, res) => {
    slashDB.student.create(req.body).then(response => {
        res.status(201).send({
            message: "Student created"
        });
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

module.exports.get = getRequestHandler;
module.exports.getSingle = getSingleRequstHandler;
module.exports.post = postRequestHandler;
