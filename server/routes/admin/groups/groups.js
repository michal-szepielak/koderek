"use strict";

const slashDB = require("../../../lib/services/slashDB");

/* GET groups list. */
const getRequestHandler = (req, res) => {
    slashDB.group.getAll().then(groups => {
        res.send(groups);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const postRequestHandler = (req, res) => {
    slashDB.group.create(req.body).then(response => {
        console.dir(response);
        res.status(201).send({
            message: "Group created"
        });
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

const getSingleRequestHandler = (req, res) => {
    slashDB.group.get(req.params.groupId).then(group => {
        res.send(group);
    }, e => {
        res.status(500).send({
            message: "internal error",
            stack: e
        });
    });
};

module.exports.get = getRequestHandler;
module.exports.post = postRequestHandler;
module.exports.getSingle = getSingleRequestHandler;