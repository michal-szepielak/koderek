"use strict";

const express = require('express');
const router = express.Router();

const courses = require("./courses/courses");
const students = require("./students/students");
const parents = require("./parents/parents");
const groups = require("./groups/groups");

/**
 * i.e.: {
        "slots": 10,
        "description": "Lorem ipsum dolor sit amet",
        "host": "Joanna Joannowicz",
        "rate": "Kurs darmowy.",
        "stopTime": "2017-10-14T00:00:00",
        "weekDay": 4,
        "startTime": "2017-10-14T00:00:00",
        "signUpEndDate": "2017-10-14T00:00:00",
        "signUpStartDate": "2017-10-10T00:00:00",
        "name": "Piłka z Szefunciem"
    }
 */
router.post("/courses", courses.post);
router.get("/courses", courses.get);
router.get("/courses/:courseId", courses.getSingle);

/**
 *{
    "firstName": "Jakub",
    "groupsId": 1,
    "lastName": "Jakubowski2",
    "parentId": 1
}
 */
router.post("/students", students.post);
router.get("/students", students.get);
router.get("/students/:studentId", students.getSingle);

/**
 * {
        "firstName": "Kamila",
        "lastName": "Kamilczak",
        "password": "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8",
        "email": "cos@ionnne.com"

    }
 */
router.post("/parents", parents.post);
router.get("/parents", parents.get);
router.get("/parents/:parentId", parents.getSingle);

/**
 * {
 *    name: "dupa"
 * }
 */
router.post("/groups", groups.post);
router.get("/groups", groups.get);
router.get("/groups/:groupId", groups.getSingle);

module.exports = router;
