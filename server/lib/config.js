"use strict";

const CONFIG = {
    slashDB: {
        apiKey: "cqtzokthbsyt382erovqj7270dg63eq5",
        host: "http://koderek.slashdb.com/db/team1/"
    }
};

module.exports.getSlashDBApiKey = () => {
    return CONFIG.slashDB.apiKey;
};

module.exports.getSlashDBHost = () => {
    return CONFIG.slashDB.host;
};
