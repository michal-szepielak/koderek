"use strict";

class Group {
    constructor(options) {
        this._id = options.id;
        this._name = options.name;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}

module.exports = Group;