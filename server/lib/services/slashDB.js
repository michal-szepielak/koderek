"use strict";

module.exports = {
    group: require("./slashDB/group"),
    parent: require("./slashDB/parent"),
    student: require("./slashDB/student"),
    course: require("./slashDB/course")
};
