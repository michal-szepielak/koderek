"use strict";

const config = require("../../config");
const request = require("request-promise");

const apiKey = config.getSlashDBApiKey();
const host = config.getSlashDBHost();

const student = {
    get: id => {
        return request.get({
            url: host + "students/id/" + id + ".json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAllForParent: parentId => {
        return request.get({
            url: host + "students/parentId/" + parentId + ".json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAll: () => {
        return request.get({
            url: host + "students.json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getParentId: studentId => {
        return request.get({
            url: host + "students/id/" + studentId + "/parentId.json",
            headers: {
                "apikey": apiKey
            }
        });
    },

    create: (options) => {
        return request.post({
            url: host + "students.json",
            headers: {
                "apikey": apiKey
            },
            body: options,
            json: true
        });
    }
};

module.exports = student;
