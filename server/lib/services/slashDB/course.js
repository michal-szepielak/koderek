"use strict";

const config = require("../../config");
const request = require("request-promise");
const syncRequest = require("sync-request");

const apiKey = config.getSlashDBApiKey();
const host = config.getSlashDBHost();

const course = {
    get: id => {
        return request.get({
            url: host + "courses/id/" + id + ".json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAll: () => {
        return request.get({
            url: host + "courses.json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAllForStudent: studentId => {
        return request.get({
            url: host + "students/id/" + studentId + "/participants/courses.json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAllIdsForStudent: studentId => {
        return request.get({
            url: host + "students/id/" + studentId + "/participants/courses/id.json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAvailableForStudentGroup: studentId => {
        return request.get({
            url: host + "students/id/" + studentId + "/groups/courseGroups/courses.json?depth=1",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    create: (options) => {
        return request.post({
            url: host + "courses.json",
            headers: {
                "apikey": apiKey
            },
            body: options,
            json: true
        });
    },

    // sync methods - don't use unless you need a transaction!
    getSlotAvailability: courseId => {
        const course = JSON.parse(syncRequest(
            'GET',
            host + "courses/id/" + courseId + ".json?depth=1",
            {
            'headers': {
                'apikey': apiKey
            }
        }).getBody().toString());

        return course.participants.reduce((prev, participant) => {
            if (participant.type === 0) {
                prev.availableMainSlots--;
            }
            if (participant.type === 1) {
                prev.availableQueueSlots--;
            }

            return prev;
        }, {
            totalSlots: course.slots * 2,
            slots: course.slots,
            availableMainSlots: course.slots,
            availableQueueSlots: course.slots
        });
    },
    signUpToMainSlot: (courseId, studentId) => {
        syncRequest(
            "POST",
            host + "participants.json",
            {
                json: {
                    studentsId: studentId,
                    coursesId: courseId,
                    type: 0
                },
                'headers': {
                    'apikey': apiKey
                }
            }
        )
    },
    signUpToQueueSlot: (courseId, studentId) => {
        syncRequest(
            "POST",
            host + "participants.json",
            {
                json: {
                    studentsId: studentId,
                    coursesId: courseId,
                    type: 1
                },
                'headers': {
                    'apikey': apiKey
                }
            }
        )
    },
};

module.exports = course;
