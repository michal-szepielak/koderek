"use strict";

const config = require("../../config");
const request = require("request-promise");

const apiKey = config.getSlashDBApiKey();
const host = config.getSlashDBHost();

const group = {
    get: id => {
        return request.get({
            url: host + "groups/id/" + id + ".json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAll: () => {
        return request.get({
            url: host + "groups.json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    create: (options) => {
        return request.post({
            url: host + "groups.json",
            headers: {
                "apikey": apiKey
            },
            body: options,
            json: true
        });
    }
};

module.exports = group;
