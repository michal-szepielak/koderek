"use strict";

const config = require("../../config");
const request = require("request-promise");

const apiKey = config.getSlashDBApiKey();
const host = config.getSlashDBHost();

const parent = {
    get: id => {
        return request.get({
            url: host + "parents/id/" + id + ".json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAll: () => {
        return request.get({
            url: host + "parents.json",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    getAllWithStudents: () => {
        return request.get({
            url: host + "parents.json?depth=1",
            headers: {
                "apikey": apiKey
            }
        }).then(responseData => {
            return JSON.parse(responseData);
        });
    },
    create: (options) => {
        return request.post({
            url: host + "parents.json",
            headers: {
                "apikey": apiKey
            },
            body: options,
            json: true
        });
    }
};

module.exports = parent;
